# Papers valuable for sharing
- [x] (NIPS'20) **Neural sparse voxel fields**
  * Yin: [Notes](https://yindaheng98.github.io/%E5%9B%BE%E5%BD%A2%E5%AD%A6/Nerf%E5%8A%A0%E9%80%9F.html#nips-20-neural-sparse-voxel-fields)
- [x] (ACM TC 2022) **Instant neural graphics primitives with a multiresolution hash encoding**
  * Yin: [Notes](https://yindaheng98.github.io/%E5%9B%BE%E5%BD%A2%E5%AD%A6/Nerf%E5%8A%A0%E9%80%9F.html#%E9%92%88%E5%AF%B9%E4%B8%8A%E8%BF%B0%E5%8A%A3%E5%8A%BF%E8%BF%9B%E4%B8%80%E6%AD%A5%E4%BC%98%E5%8C%96-acm-tc-2022-instant-neural-graphics-primitives-with-a-multiresolution-hash-encoding), Idea来源于[空间哈希函数](https://yindaheng98.github.io/%E5%9B%BE%E5%BD%A2%E5%AD%A6/%E7%A9%BA%E9%97%B4%E5%93%88%E5%B8%8C.html)
- [ ] (SIGGRAPH '22) **Variable Bitrate Neural Fields**
- [ ] (CVPR 2022) **Mega-NERF: Scalable Construction of Large-Scale NeRFs for Virtual Fly-Throughs**
- [ ] (CVPR 2021) **KiloNeRF: Speeding Up Neural Radiance Fields With Thousands of Tiny MLPs**
- [x] (CVPR 2021) **Neural Geometric Level of Detail: Real-Time Rendering With Implicit 3D Shapes**
  * Yin: [Notes](https://yindaheng98.github.io/%E5%9B%BE%E5%BD%A2%E5%AD%A6/NeuralSDF.html#cvpr-2021-neural-geometric-level-of-detail-real-time-rendering-with-implicit-3d-shapes)
- [x] (CVPR 2020) **Implicit Functions in Feature Space for 3D Shape Reconstruction and Completion**
  * Yin: [Notes](https://yindaheng98.github.io/%E5%9B%BE%E5%BD%A2%E5%AD%A6/NeuralSDF.html#cvpr-2020-implicit-functions-in-feature-space-for-3d-shape-reconstruction-and-completion)
- [ ] (ACM TG 2023) **3D Gaussian Splatting for Real-Time Radiance Field Rendering**
- [x] (NSDI 22) **YuZu: Neural-Enhanced Volumetric Video Streaming**
- [ ] (VR 2023) **CaV3: Cache-assisted Viewport Adaptive Volumetric Video Streaming**
- [ ] (ImmerCom '23) **Toward Next-generation Volumetric Video Streaming with Neural-based Content Representations**
  * model compression, rate adaptation with scalable neural networks, and viewport adaptation to alleviate bandwidth consumption
